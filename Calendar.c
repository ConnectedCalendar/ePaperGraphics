/**
 *******************************************************************************
 * @file    Calendar.c
 * @brief   List of calendar events
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <malloc.h>
#include <string.h>
#include <math.h>
#include "Calendar.h"

/* Private variables */
static event_t *calendar = NULL;
static tm_t currentTime;

/* Private functions */
static event_t *FindLastEvent(void);

/******************************************************************************/

/**
 * @brief Add an event to the calendar list
 * @param title summary of the event
 * @param start time and date of start
 * @param end time and date of end
 * @param end time and date of end
 */
void NewEvent(const char *title, tm_t start, tm_t end) {
    event_t *newEvent = (event_t *) malloc(sizeof(struct event));
    strcpy(newEvent->title, title);
    newEvent->start = start;
    newEvent->end = end;
    newEvent->next = NULL;

    event_t *lastEvent = FindLastEvent();
    if (lastEvent == NULL) {
        calendar = newEvent;
    } else {
        lastEvent->next = newEvent;
    }
}

/**
 * @brief Deletes all calendar entries
 */
void ClearCalendar(void) {
    event_t *event = calendar;
    event_t *next;

    while (event != NULL) {
        next = event->next;
        free(event);
        event = next;
    }
}

/**
 * @brief Get the list head of the calendar
 * @return first element of the calendar list
 */
event_t *GetCalendar(void) {
    return calendar;
}

/**
 * @brief Set time and date of current date
 * @param time time and date
 */
void SetCurrentTime(tm_t time) {
    currentTime = time;
}

/**
 * @brief Get time and date calendar was synced
 * @return time and date
 */
tm_t GetCurrentTime(void) {
    return currentTime;
}

/**
 * @brief Calculates the time difference of two events in days (integer)
 * @param start first date
 * @param end second date
 * @return difference (end - start) in days (integer)
 */
int DiffDays(tm_t *start, tm_t *end) {
    return (int) floor(difftime(mktime(end), mktime(start)) / SECONDS_PER_DAY);
}

/**
 * @brief Converts UTC to central european time (CET) including daylight saving
 * @param time UTC time and date, will be modified to CET
 *
 * Daylight saving time is from the last sunday of march to the last sunday of
 * october
 */
void ConvertUtcToCet(tm_t *tm) {
    // evaluate dates of daylight saving time
    tm_t beginDS, endDS;
    beginDS.tm_year = tm->tm_year;
    beginDS.tm_mon = MARCH;
    beginDS.tm_mday = 31;
    beginDS.tm_hour = 0;
    beginDS.tm_min = 0;
    beginDS.tm_sec = 0;
    mktime(&beginDS);
    beginDS.tm_mday -= beginDS.tm_wday;


    endDS.tm_year = tm->tm_year;
    endDS.tm_mon = OCTOBER;
    endDS.tm_mday = 31;
    endDS.tm_hour = 0;
    endDS.tm_min = 0;
    endDS.tm_sec = 0;
    mktime(&endDS);
    endDS.tm_mday -= endDS.tm_wday + 1;

    mktime(tm);
    if ((DiffDays(tm, &beginDS) > 0) && (DiffDays(tm, &endDS) < 0)) {
        tm->tm_hour += 2;
    } else {
        tm->tm_hour += 1;
    }
    mktime(tm);
}

/**
 * @brief Find the the last event in the calendar list
 * @return last element
 */
static event_t *FindLastEvent(void) {
    event_t *event = calendar;

    if (event != NULL) {
        while (event->next != NULL) {
            event = event->next;
        }

    }

    return event;
}

/******************************************************************************/