#ifndef CALENDAR_H
#define CALENDAR_H
/**
 *******************************************************************************
 * @file Calendar.h
 *******************************************************************************
 */
#include <time.h>

/* Constants */
#define SECONDS_PER_DAY     86400.0
#define YEARS_OFFSET        1900
#define MONTHS_OFFSET       1
enum {JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER,
    OCTOBER, NOVEMBER, DECEMBER};

/* Types */
typedef struct tm tm_t;

typedef struct event {
    char title[30];
    tm_t start;
    tm_t end;

    struct event *next;
} event_t;

/* Functions */
void NewEvent(const char *title, tm_t start, tm_t end);
void ClearCalendar(void);
event_t *GetCalendar(void);
void SetCurrentTime(tm_t time);
tm_t GetCurrentTime(void);
int DiffDays(tm_t *start, tm_t *end);
void ConvertUtcToCet(tm_t *time);

/******************************************************************************/
#endif /* ifndef CALENDAR_H */
