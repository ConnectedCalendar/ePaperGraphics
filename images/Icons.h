#ifndef ICONS_H
#define ICONS_H
/**
 *******************************************************************************
 * @file Icons.h
 *******************************************************************************
 */

#include <stdint.h>

/* Constants */

/* Types */
typedef struct {
    uint8_t height;
    uint8_t width;
    char *bitmap;
} icon_t;

extern const icon_t ICON_UPDATE;

/******************************************************************************/
#endif /* ifndef ICONS_H */
