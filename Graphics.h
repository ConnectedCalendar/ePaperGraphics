#ifndef GRAPHICS_H
#define GRAPHICS_H
/**
 *******************************************************************************
 * @file Graphics.h
 *******************************************************************************
 */
#include <stdint.h>
#include "Font/Font.h"
#include "images/Icons.h"

/* Constants */
#define RES_X           1024
#define RES_Y           1280
#define RES_CELLS_X     128 // 1024/8
#define RES_CELLS       163840 // 1024/8 * 1280
#define LINE_W          3
#define DIR_HORIZONTAL  0
#define DIR_VERTICAL    1

/* Types */
typedef enum {FLUSH_LEFT, FLUSH_RIGHT} flush_t;


/* Functions */
uint8_t *GetFramebuffer(void);
void DrawLine(uint32_t x, uint32_t y, uint8_t direction, uint32_t length);
void DrawRectangle(uint32_t x, uint32_t y, uint32_t width, uint32_t height);
void InvertArea(uint32_t x, uint32_t y, uint32_t width, uint32_t height);
void PrintString(uint32_t x, uint32_t y, const char *str, const font_t *font,
                 flush_t alignment);
void PlaceIcon(uint32_t x, uint32_t y, const icon_t *icon);
void ClearFramebuffer(void);

/******************************************************************************/
#endif /* ifndef GRAPHICS_H */
