/**
 *******************************************************************************
 * @file    Parser.c
 * @brief   Parses the data from the WiFi module
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Parser.h"
#include "Calendar.h"
#include "UI.h"

/* Constants */
const char *PAR_NTP = "NTP:";
const char *PAR_NTP_FORMAT = "NTP:%4d%2d%2dT%2d%2d%2d";

const char *PAR_EVENT_BEGIN = "BEGIN:VEVENT";
const char *PAR_EVENT_END = "END:VEVENT";
const char *PAR_EVENT_TITLE = "SUMMARY:";
const char *PAR_EVENT_TITLE_FORMAT = "SUMMARY:%s";
const char *PAR_EVENT_DSTART = "DTSTART;";
const char *PAR_EVENT_DEND = "DTEND;";
const char *PAR_EVENT_DAY = "VALUE=DATE";
const char *PAR_EVENT_DAY_START_FORMAT = "DTSTART;VALUE=DATE:%4d%2d%2d";
const char *PAR_EVENT_DAY_END_FORMAT = "DTEND;VALUE=DATE:%4d%2d%2d";
const char *PAR_EVENT_DAY_TIME = "TZID=";
const char *PAR_EVENT_DAY_TIME_START_FORMAT =
        "DTSTART;TZID=Europe/Zurich:%4d%2d%2dT%2d%2d%2d";
const char *PAR_EVENT_DAY_TIME_END_FORMAT =
        "DTEND;TZID=Europe/Zurich:%4d%2d%2dT%2d%2d%2d";



const char *PAR_TELEGRAM_BEGIN = "BEGIN:TELEGRAM";
const char *PAR_TELEGRAM_END = "END:TELEGRAM";

const char *PAR_TRANSMISSON_END = "DEEPSLEEP";

/* Private variables */
static enum {
    S_TOP, S_EVENT, S_TELEGRAM
} state = S_TOP;

/* Private functions */
static bool FindInString(const char *source, const char *key);

/******************************************************************************/

bool Parse(char *line) {
    static tm_t start, end;
    static char *title[60];
    int year, month, day, hour, minute, second;
    tm_t ntpTime;

    if (FindInString(line, PAR_TRANSMISSON_END)) {
        state = S_TOP;
        return true;
    }

    switch (state) {
        case S_TOP:
            // NTP time and date
            if (FindInString(line, PAR_NTP)) {
                sscanf(line, PAR_NTP_FORMAT,
                       &year, &month, &day, &hour, &minute, &second);
                ntpTime.tm_year = year - YEARS_OFFSET;
                ntpTime.tm_mon = month - MONTHS_OFFSET;
                ntpTime.tm_mday = day;
                ntpTime.tm_hour = hour;
                ntpTime.tm_min = minute;
                ntpTime.tm_sec = second;
                ConvertUtcToCet(&ntpTime);
                SetCurrentTime(ntpTime);
                break;
            }

            // Calendar Event
            if (FindInString(line, PAR_EVENT_BEGIN)) {
                state = S_EVENT;
                break;
            }

            // Telegram message
            if (FindInString(line, PAR_TELEGRAM_BEGIN)) {
                state = S_TELEGRAM;
                break;
            }
            break;

        case S_EVENT:
            if (FindInString(line, PAR_EVENT_TITLE)) {
                sscanf(line, PAR_EVENT_TITLE_FORMAT,
                       title);
                break;
            }

            if (FindInString(line, PAR_EVENT_DSTART)) {
                // Event takes >1 full day
                if (FindInString(line, PAR_EVENT_DAY)) {
                    sscanf(line, PAR_EVENT_DAY_START_FORMAT,
                           &year, &month, &day);
                    start.tm_year = year - YEARS_OFFSET;
                    start.tm_mon = month - MONTHS_OFFSET;
                    start.tm_mday = day;
                    start.tm_hour = 0;
                    start.tm_min = 0;
                    start.tm_sec = 0;
                    mktime(&start);
                    break;
                }

                // Event takes <1 full day
                if (FindInString(line, PAR_EVENT_DAY_TIME)) {
                    sscanf(line, PAR_EVENT_DAY_TIME_START_FORMAT,
                           &year, &month, &day, &hour, &minute, &second);
                    start.tm_year = year - YEARS_OFFSET;
                    start.tm_mon = month - MONTHS_OFFSET;
                    start.tm_mday = day;
                    start.tm_hour = hour;
                    start.tm_min = minute;
                    start.tm_sec = second;
                    mktime(&start);
                    break;
                }
            }

            if (FindInString(line, PAR_EVENT_DEND)) {
                // Event takes >1 full day
                if (FindInString(line, PAR_EVENT_DAY)) {
                    sscanf(line, PAR_EVENT_DAY_END_FORMAT,
                           &year, &month, &day);
                    end.tm_year = year - YEARS_OFFSET;
                    end.tm_mon = month - MONTHS_OFFSET;
                    end.tm_mday = day;
                    end.tm_hour = 0;
                    end.tm_min = 0;
                    end.tm_sec = 0;
                    mktime(&end);
                    break;
                }

                // Event takes <1 full day
                if (FindInString(line, PAR_EVENT_DAY_TIME)) {
                    sscanf(line, PAR_EVENT_DAY_TIME_END_FORMAT,
                           &year, &month, &day, &hour, &minute, &second);
                    end.tm_year = year - YEARS_OFFSET;
                    end.tm_mon = month - MONTHS_OFFSET;
                    end.tm_mday = day;
                    end.tm_hour = hour;
                    end.tm_min = minute;
                    end.tm_sec = second;
                    mktime(&end);
                    break;
                }
            }

            if (FindInString(line, PAR_EVENT_END)) {
                NewEvent((const char *)title, start, end);
                state = S_TOP;
                break;
            }
            break;

        case S_TELEGRAM:
            ntpTime = GetCurrentTime();
            ShowMessage(&ntpTime, line);
            state = S_TOP;
            break;
    }

    return false;
}

/**
 * @brief Finds a key in a string
 * @param source string to be searched
 * @param key string to be found
 * @return found: true, not found: false
 */
static bool FindInString(const char *source, const char *key) {
    uint32_t i = 0, match = 0;

    while (1) {
        if ((source[i] == '\0') || (match == strlen(key) - 1)) {
            break;
        }

        if (source[i] == key[match]) {
            match++;
        } else {
            match = 0;
        }
        i++;
    }

    if (match < strlen(key) - 1) {
        return false;
    } else {
        return true;
    }
}

/******************************************************************************/