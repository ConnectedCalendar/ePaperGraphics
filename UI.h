#ifndef UI_H
#define UI_H
/**
 *******************************************************************************
 * @file UI.h
 *******************************************************************************
 */

#include "Calendar.h"

/* Constants */
#define BORDER          10      // border around display content

#define CAL_SPACING     10       // inner border in calendar
#define CAL_CELL_SIZE   142     // cell size within calendar display
#define CAL_DATE_X      130     // x offset for date placement in calendar
#define CAL_DATE_Y      90

#define WEEK_DAYS       7
#define WEEKS           2       // weeks displayed in the calendar

#define EVENT_MARKER    33      // event marker size
#define EVENTS_MAX      8       // max number of events displayable

#define LIST_EVENTS_MAX 6
#define LIST_START_Y    310
#define LIST_LINE_W     8
#define LIST_LINE_H     97
#define LIST_LINE_OFF   16
#define LIST_SEP_Y      120

#define MSG_START_Y     1070

/* Types */

/* Functions */
void DrawCalendar (void);
void ListEvents(void);
void ShowUpdateTime(void);
void ShowMessage(tm_t *time, char *message);

/******************************************************************************/
#endif /* ifndef UI_H */
