#ifndef FONT_H
#define FONT_H
/**
 *******************************************************************************
 * @file Font.h
 *******************************************************************************
 */

#include <stdint.h>


/* Constants */
#define CHAR_OFFSET     ((char) 32)
#define FONT_ENTRIES    143

/* Types */
typedef struct {
    uint8_t width;
    char *bitmap;
} character_t;

typedef struct {
    uint8_t height;
    character_t c[FONT_ENTRIES];
} font_t;

extern const font_t FONT_HEADING;
extern const font_t FONT_TEXT;

/* Functions */
char CharRemap(char c);

/******************************************************************************/
#endif /* ifndef FONT_H */
