/**
 *******************************************************************************
 * @file    FontHeading.c
 * @brief   Font constant composing all characters
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, hopy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include "../Font.h"

/* Includes */
#include "h32.xbm"
#include "h33.xbm"
#include "h34.xbm"
#include "h35.xbm"
#include "h36.xbm"
#include "h37.xbm"
#include "h38.xbm"
#include "h39.xbm"
#include "h40.xbm"
#include "h41.xbm"
#include "h42.xbm"
#include "h43.xbm"
#include "h44.xbm"
#include "h45.xbm"
#include "h46.xbm"
#include "h47.xbm"
#include "h48.xbm"
#include "h49.xbm"
#include "h50.xbm"
#include "h51.xbm"
#include "h52.xbm"
#include "h53.xbm"
#include "h54.xbm"
#include "h55.xbm"
#include "h56.xbm"
#include "h57.xbm"
#include "h58.xbm"
#include "h59.xbm"
#include "h60.xbm"
#include "h61.xbm"
#include "h62.xbm"
#include "h63.xbm"
#include "h64.xbm"
#include "h65.xbm"
#include "h66.xbm"
#include "h67.xbm"
#include "h68.xbm"
#include "h69.xbm"
#include "h70.xbm"
#include "h71.xbm"
#include "h72.xbm"
#include "h73.xbm"
#include "h74.xbm"
#include "h75.xbm"
#include "h76.xbm"
#include "h77.xbm"
#include "h78.xbm"
#include "h79.xbm"
#include "h80.xbm"
#include "h81.xbm"
#include "h82.xbm"
#include "h83.xbm"
#include "h84.xbm"
#include "h85.xbm"
#include "h86.xbm"
#include "h87.xbm"
#include "h88.xbm"
#include "h89.xbm"
#include "h90.xbm"
#include "h91.xbm"
#include "h92.xbm"
#include "h93.xbm"
#include "h94.xbm"
#include "h95.xbm"
#include "h96.xbm"
#include "h97.xbm"
#include "h98.xbm"
#include "h99.xbm"
#include "h100.xbm"
#include "h101.xbm"
#include "h102.xbm"
#include "h103.xbm"
#include "h104.xbm"
#include "h105.xbm"
#include "h106.xbm"
#include "h107.xbm"
#include "h108.xbm"
#include "h109.xbm"
#include "h110.xbm"
#include "h111.xbm"
#include "h112.xbm"
#include "h113.xbm"
#include "h114.xbm"
#include "h115.xbm"
#include "h116.xbm"
#include "h117.xbm"
#include "h118.xbm"
#include "h119.xbm"
#include "h120.xbm"
#include "h121.xbm"
#include "h122.xbm"
#include "h123.xbm"
#include "h124.xbm"
#include "h125.xbm"
#include "h126.xbm"
#include "h127.xbm"
#include "h128.xbm"
#include "h129.xbm"
#include "h130.xbm"
#include "h131.xbm"
#include "h132.xbm"
#include "h133.xbm"
#include "h134.xbm"
#include "h135.xbm"
#include "h136.xbm"
#include "h137.xbm"
#include "h138.xbm"
#include "h139.xbm"
#include "h140.xbm"
#include "h141.xbm"
#include "h142.xbm"
#include "h143.xbm"
#include "h144.xbm"
#include "h145.xbm"
#include "h146.xbm"
#include "h147.xbm"
#include "h148.xbm"
#include "h149.xbm"
#include "h150.xbm"
#include "h151.xbm"
#include "h152.xbm"
#include "h153.xbm"
#include "h154.xbm"
#include "h155.xbm"
#include "h156.xbm"
#include "h157.xbm"
#include "h158.xbm"
#include "h159.xbm"
#include "h160.xbm"
#include "h161.xbm"
#include "h162.xbm"
#include "h163.xbm"
#include "h164.xbm"
#include "h165.xbm"
#include "h166.xbm"
#include "h167.xbm"
#include "h168.xbm"
#include "h169.xbm"
#include "h170.xbm"
#include "h171.xbm"
#include "h172.xbm"
#include "h173.xbm"
#include "h174.xbm"

/* Global constants */
const font_t FONT_HEADING = {
        h65_height,
        {
                {h32_width, h32_bits},
                {h33_width, h33_bits},
                {h34_width, h34_bits},
                {h35_width, h35_bits},
                {h36_width, h36_bits},
                {h37_width, h37_bits},
                {h38_width, h38_bits},
                {h39_width, h39_bits},
                {h40_width, h40_bits},
                {h41_width, h41_bits},
                {h42_width, h42_bits},
                {h43_width, h43_bits},
                {h44_width, h44_bits},
                {h45_width, h45_bits},
                {h46_width, h46_bits},
                {h47_width, h47_bits},
                {h48_width, h48_bits},
                {h49_width, h49_bits},
                {h50_width, h50_bits},
                {h51_width, h51_bits},
                {h52_width, h52_bits},
                {h53_width, h53_bits},
                {h54_width, h54_bits},
                {h55_width, h55_bits},
                {h56_width, h56_bits},
                {h57_width, h57_bits},
                {h58_width, h58_bits},
                {h59_width, h59_bits},
                {h60_width, h60_bits},
                {h61_width, h61_bits},
                {h62_width, h62_bits},
                {h63_width, h63_bits},
                {h64_width, h64_bits},
                {h65_width, h65_bits},
                {h66_width, h66_bits},
                {h67_width, h67_bits},
                {h68_width, h68_bits},
                {h69_width, h69_bits},
                {h70_width, h70_bits},
                {h71_width, h71_bits},
                {h72_width, h72_bits},
                {h73_width, h73_bits},
                {h74_width, h74_bits},
                {h75_width, h75_bits},
                {h76_width, h76_bits},
                {h77_width, h77_bits},
                {h78_width, h78_bits},
                {h79_width, h79_bits},
                {h80_width, h80_bits},
                {h81_width, h81_bits},
                {h82_width, h82_bits},
                {h83_width, h83_bits},
                {h84_width, h84_bits},
                {h85_width, h85_bits},
                {h86_width, h86_bits},
                {h87_width, h87_bits},
                {h88_width, h88_bits},
                {h89_width, h89_bits},
                {h90_width, h90_bits},
                {h91_width, h91_bits},
                {h92_width, h92_bits},
                {h93_width, h93_bits},
                {h94_width, h94_bits},
                {h95_width, h95_bits},
                {h96_width, h96_bits},
                {h97_width, h97_bits},
                {h98_width, h98_bits},
                {h99_width, h99_bits},
                {h100_width, h100_bits},
                {h101_width, h101_bits},
                {h102_width, h102_bits},
                {h103_width, h103_bits},
                {h104_width, h104_bits},
                {h105_width, h105_bits},
                {h106_width, h106_bits},
                {h107_width, h107_bits},
                {h108_width, h108_bits},
                {h109_width, h109_bits},
                {h110_width, h110_bits},
                {h111_width, h111_bits},
                {h112_width, h112_bits},
                {h113_width, h113_bits},
                {h114_width, h114_bits},
                {h115_width, h115_bits},
                {h116_width, h116_bits},
                {h117_width, h117_bits},
                {h118_width, h118_bits},
                {h119_width, h119_bits},
                {h120_width, h120_bits},
                {h121_width, h121_bits},
                {h122_width, h122_bits},
                {h123_width, h123_bits},
                {h124_width, h124_bits},
                {h125_width, h125_bits},
                {h126_width, h126_bits},
                {h127_width, h127_bits},
                {h128_width, h128_bits},
                {h129_width, h129_bits},
                {h130_width, h130_bits},
                {h131_width, h131_bits},
                {h132_width, h132_bits},
                {h133_width, h133_bits},
                {h134_width, h134_bits},
                {h135_width, h135_bits},
                {h136_width, h136_bits},
                {h137_width, h137_bits},
                {h138_width, h138_bits},
                {h139_width, h139_bits},
                {h140_width, h140_bits},
                {h141_width, h141_bits},
                {h142_width, h142_bits},
                {h143_width, h143_bits},
                {h144_width, h144_bits},
                {h145_width, h145_bits},
                {h146_width, h146_bits},
                {h147_width, h147_bits},
                {h148_width, h148_bits},
                {h149_width, h149_bits},
                {h150_width, h150_bits},
                {h151_width, h151_bits},
                {h152_width, h152_bits},
                {h153_width, h153_bits},
                {h154_width, h154_bits},
                {h155_width, h155_bits},
                {h156_width, h156_bits},
                {h157_width, h157_bits},
                {h158_width, h158_bits},
                {h159_width, h159_bits},
                {h160_width, h160_bits},
                {h161_width, h161_bits},
                {h162_width, h162_bits},
                {h163_width, h163_bits},
                {h164_width, h164_bits},
                {h165_width, h165_bits},
                {h166_width, h166_bits},
                {h167_width, h167_bits},
                {h168_width, h168_bits},
                {h169_width, h169_bits},
                {h170_width, h170_bits},
                {h171_width, h171_bits},
                {h172_width, h172_bits},
                {h173_width, h173_bits},
                {h174_width, h174_bits}
        }
};

/******************************************************************************/
