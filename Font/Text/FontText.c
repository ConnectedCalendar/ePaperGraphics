/**
 *******************************************************************************
 * @file    FontHeading.c
 * @brief   Font constant composing all characters
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, topy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include "../Font.h"

/* Includes */
#include "t32.xbm"
#include "t33.xbm"
#include "t34.xbm"
#include "t35.xbm"
#include "t36.xbm"
#include "t37.xbm"
#include "t38.xbm"
#include "t39.xbm"
#include "t40.xbm"
#include "t41.xbm"
#include "t42.xbm"
#include "t43.xbm"
#include "t44.xbm"
#include "t45.xbm"
#include "t46.xbm"
#include "t47.xbm"
#include "t48.xbm"
#include "t49.xbm"
#include "t50.xbm"
#include "t51.xbm"
#include "t52.xbm"
#include "t53.xbm"
#include "t54.xbm"
#include "t55.xbm"
#include "t56.xbm"
#include "t57.xbm"
#include "t58.xbm"
#include "t59.xbm"
#include "t60.xbm"
#include "t61.xbm"
#include "t62.xbm"
#include "t63.xbm"
#include "t64.xbm"
#include "t65.xbm"
#include "t66.xbm"
#include "t67.xbm"
#include "t68.xbm"
#include "t69.xbm"
#include "t70.xbm"
#include "t71.xbm"
#include "t72.xbm"
#include "t73.xbm"
#include "t74.xbm"
#include "t75.xbm"
#include "t76.xbm"
#include "t77.xbm"
#include "t78.xbm"
#include "t79.xbm"
#include "t80.xbm"
#include "t81.xbm"
#include "t82.xbm"
#include "t83.xbm"
#include "t84.xbm"
#include "t85.xbm"
#include "t86.xbm"
#include "t87.xbm"
#include "t88.xbm"
#include "t89.xbm"
#include "t90.xbm"
#include "t91.xbm"
#include "t92.xbm"
#include "t93.xbm"
#include "t94.xbm"
#include "t95.xbm"
#include "t96.xbm"
#include "t97.xbm"
#include "t98.xbm"
#include "t99.xbm"
#include "t100.xbm"
#include "t101.xbm"
#include "t102.xbm"
#include "t103.xbm"
#include "t104.xbm"
#include "t105.xbm"
#include "t106.xbm"
#include "t107.xbm"
#include "t108.xbm"
#include "t109.xbm"
#include "t110.xbm"
#include "t111.xbm"
#include "t112.xbm"
#include "t113.xbm"
#include "t114.xbm"
#include "t115.xbm"
#include "t116.xbm"
#include "t117.xbm"
#include "t118.xbm"
#include "t119.xbm"
#include "t120.xbm"
#include "t121.xbm"
#include "t122.xbm"
#include "t123.xbm"
#include "t124.xbm"
#include "t125.xbm"
#include "t126.xbm"
#include "t127.xbm"
#include "t128.xbm"
#include "t129.xbm"
#include "t130.xbm"
#include "t131.xbm"
#include "t132.xbm"
#include "t133.xbm"
#include "t134.xbm"
#include "t135.xbm"
#include "t136.xbm"
#include "t137.xbm"
#include "t138.xbm"
#include "t139.xbm"
#include "t140.xbm"
#include "t141.xbm"
#include "t142.xbm"
#include "t143.xbm"
#include "t144.xbm"
#include "t145.xbm"
#include "t146.xbm"
#include "t147.xbm"
#include "t148.xbm"
#include "t149.xbm"
#include "t150.xbm"
#include "t151.xbm"
#include "t152.xbm"
#include "t153.xbm"
#include "t154.xbm"
#include "t155.xbm"
#include "t156.xbm"
#include "t157.xbm"
#include "t158.xbm"
#include "t159.xbm"
#include "t160.xbm"
#include "t161.xbm"
#include "t162.xbm"
#include "t163.xbm"
#include "t164.xbm"
#include "t165.xbm"
#include "t166.xbm"
#include "t167.xbm"
#include "t168.xbm"
#include "t169.xbm"
#include "t170.xbm"
#include "t171.xbm"
#include "t172.xbm"
#include "t173.xbm"
#include "t174.xbm"

/* Global constants */
const font_t FONT_TEXT = {
        t65_height,
        {
                {t32_width, t32_bits},
                {t33_width, t33_bits},
                {t34_width, t34_bits},
                {t35_width, t35_bits},
                {t36_width, t36_bits},
                {t37_width, t37_bits},
                {t38_width, t38_bits},
                {t39_width, t39_bits},
                {t40_width, t40_bits},
                {t41_width, t41_bits},
                {t42_width, t42_bits},
                {t43_width, t43_bits},
                {t44_width, t44_bits},
                {t45_width, t45_bits},
                {t46_width, t46_bits},
                {t47_width, t47_bits},
                {t48_width, t48_bits},
                {t49_width, t49_bits},
                {t50_width, t50_bits},
                {t51_width, t51_bits},
                {t52_width, t52_bits},
                {t53_width, t53_bits},
                {t54_width, t54_bits},
                {t55_width, t55_bits},
                {t56_width, t56_bits},
                {t57_width, t57_bits},
                {t58_width, t58_bits},
                {t59_width, t59_bits},
                {t60_width, t60_bits},
                {t61_width, t61_bits},
                {t62_width, t62_bits},
                {t63_width, t63_bits},
                {t64_width, t64_bits},
                {t65_width, t65_bits},
                {t66_width, t66_bits},
                {t67_width, t67_bits},
                {t68_width, t68_bits},
                {t69_width, t69_bits},
                {t70_width, t70_bits},
                {t71_width, t71_bits},
                {t72_width, t72_bits},
                {t73_width, t73_bits},
                {t74_width, t74_bits},
                {t75_width, t75_bits},
                {t76_width, t76_bits},
                {t77_width, t77_bits},
                {t78_width, t78_bits},
                {t79_width, t79_bits},
                {t80_width, t80_bits},
                {t81_width, t81_bits},
                {t82_width, t82_bits},
                {t83_width, t83_bits},
                {t84_width, t84_bits},
                {t85_width, t85_bits},
                {t86_width, t86_bits},
                {t87_width, t87_bits},
                {t88_width, t88_bits},
                {t89_width, t89_bits},
                {t90_width, t90_bits},
                {t91_width, t91_bits},
                {t92_width, t92_bits},
                {t93_width, t93_bits},
                {t94_width, t94_bits},
                {t95_width, t95_bits},
                {t96_width, t96_bits},
                {t97_width, t97_bits},
                {t98_width, t98_bits},
                {t99_width, t99_bits},
                {t100_width, t100_bits},
                {t101_width, t101_bits},
                {t102_width, t102_bits},
                {t103_width, t103_bits},
                {t104_width, t104_bits},
                {t105_width, t105_bits},
                {t106_width, t106_bits},
                {t107_width, t107_bits},
                {t108_width, t108_bits},
                {t109_width, t109_bits},
                {t110_width, t110_bits},
                {t111_width, t111_bits},
                {t112_width, t112_bits},
                {t113_width, t113_bits},
                {t114_width, t114_bits},
                {t115_width, t115_bits},
                {t116_width, t116_bits},
                {t117_width, t117_bits},
                {t118_width, t118_bits},
                {t119_width, t119_bits},
                {t120_width, t120_bits},
                {t121_width, t121_bits},
                {t122_width, t122_bits},
                {t123_width, t123_bits},
                {t124_width, t124_bits},
                {t125_width, t125_bits},
                {t126_width, t126_bits},
                {t127_width, t127_bits},
                {t128_width, t128_bits},
                {t129_width, t129_bits},
                {t130_width, t130_bits},
                {t131_width, t131_bits},
                {t132_width, t132_bits},
                {t133_width, t133_bits},
                {t134_width, t134_bits},
                {t135_width, t135_bits},
                {t136_width, t136_bits},
                {t137_width, t137_bits},
                {t138_width, t138_bits},
                {t139_width, t139_bits},
                {t140_width, t140_bits},
                {t141_width, t141_bits},
                {t142_width, t142_bits},
                {t143_width, t143_bits},
                {t144_width, t144_bits},
                {t145_width, t145_bits},
                {t146_width, t146_bits},
                {t147_width, t147_bits},
                {t148_width, t148_bits},
                {t149_width, t149_bits},
                {t150_width, t150_bits},
                {t151_width, t151_bits},
                {t152_width, t152_bits},
                {t153_width, t153_bits},
                {t154_width, t154_bits},
                {t155_width, t155_bits},
                {t156_width, t156_bits},
                {t157_width, t157_bits},
                {t158_width, t158_bits},
                {t159_width, t159_bits},
                {t160_width, t160_bits},
                {t161_width, t161_bits},
                {t162_width, t162_bits},
                {t163_width, t163_bits},
                {t164_width, t164_bits},
                {t165_width, t165_bits},
                {t166_width, t166_bits},
                {t167_width, t167_bits},
                {t168_width, t168_bits},
                {t169_width, t169_bits},
                {t170_width, t170_bits},
                {t171_width, t171_bits},
                {t172_width, t172_bits},
                {t173_width, t173_bits},
                {t174_width, t174_bits}
        }
};

/******************************************************************************/
