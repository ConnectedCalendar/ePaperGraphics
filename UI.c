/**
 *******************************************************************************
 * @file    UI.c
 * @brief   Draws the calendar and text
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <stdio.h>
#include <time.h>
#include <string.h>
#include "UI.h"
#include "Graphics.h"
#include "images/Icons.h"
#include "Font/Font.h"

/* Private variables */

/* Private functions */
static void MarkEvent(int32_t cellStart, int32_t cellEnd);

/******************************************************************************/

/**
 * @brief Draws the a simple calendar for the next n-weeks.
 * Features are:
 * - event maker (single day or over multiple days)
 * - dates are shown in the corner
 * - current date is marked
 */
void DrawCalendar(void) {
    // draw grid
    for (uint32_t i = 0; i <= WEEKS; ++i) {
        DrawLine(BORDER, BORDER + i * CAL_CELL_SIZE, DIR_HORIZONTAL,
                 WEEK_DAYS * CAL_CELL_SIZE);

    }
    for (int j = 0; j <= WEEK_DAYS; ++j) {
        DrawLine(BORDER + j * CAL_CELL_SIZE, BORDER, DIR_VERTICAL,
                 2 * CAL_CELL_SIZE + LINE_W);
    }

    // get basic time variables
    event_t *calendar = GetCalendar();
    tm_t currentTime = GetCurrentTime();
    tm_t monday = currentTime;

    // sunday special case
    if (currentTime.tm_wday == 0) {
        monday.tm_mday -= 6;
    } else {
        monday.tm_mday -= currentTime.tm_wday - 1;
    }
    monday.tm_hour = 0;
    monday.tm_min = 0;
    monday.tm_sec = 0;
    mktime(&monday);

    // place dates
    char buffer[5];
    tm_t date = monday;
    for (int k = 0; k < WEEKS; ++k) {
        for (int i = 0; i < WEEK_DAYS; ++i) {
            sprintf(buffer, "%d", date.tm_mday);
            PrintString(BORDER + i * CAL_CELL_SIZE + CAL_DATE_X,
                        BORDER + k * CAL_CELL_SIZE + CAL_DATE_Y,
                        buffer, &FONT_TEXT, FLUSH_RIGHT);

            // increment date
            date.tm_mday++;
            mktime(&date);
        }
    }

    // mark events
    while (calendar != NULL) {
        MarkEvent(DiffDays(&monday, &calendar->start),
                  DiffDays(&monday, &calendar->end));
        calendar = calendar->next;
    }

    // mark current date
    int32_t currentCell;
    if (currentTime.tm_wday == 0) {
        currentCell = 6;
    } else {
        currentCell = currentTime.tm_wday - 1;
    }
    InvertArea(BORDER + currentCell % WEEK_DAYS * CAL_CELL_SIZE + LINE_W,
               BORDER + currentCell / WEEK_DAYS * CAL_CELL_SIZE + LINE_W,
               CAL_CELL_SIZE - LINE_W, CAL_CELL_SIZE - LINE_W);
}

/**
 * @brief Lists the next n events with title, date and time
 */
void ListEvents(void) {
    uint32_t eventCount = 0;
    event_t *calendar = GetCalendar();
    char buffer[20];
    char dateString[60];

    while ((calendar != NULL) && (eventCount < LIST_EVENTS_MAX)) {
        PrintString(2*BORDER + LIST_LINE_W,
                    LIST_START_Y + eventCount*LIST_SEP_Y,
                    calendar->title,
                    &FONT_HEADING,
                    FLUSH_LEFT);

        // show time on events < 1-day
        int bla = DiffDays(&calendar->start, &calendar->end);
        if (DiffDays(&calendar->start, &calendar->end) <= 0) {
            // full single day events are marked by the start time 00:00
            if ((calendar->start.tm_hour == 0) &&
                    (calendar->start.tm_min == 0)) {
                strftime(dateString, 20, "%d.%m.", &calendar->start);
            } else {
                strftime(dateString, 20, "%d.%m.   ", &calendar->start);
                strftime(buffer, 20, "%H:%M - ", &calendar->start);
                strcat(dateString, buffer);
                strftime(buffer, 20, "%H:%M", &calendar->end);
                strcat(dateString, buffer);
            }

        } else {
            strftime(dateString, 20, "%d.%m. - ", &calendar->start);
            strftime(buffer, 20, "%d.%m.", &calendar->end);
            strcat(dateString, buffer);
        }

        PrintString(2*BORDER + LIST_LINE_W,
                    LIST_START_Y + eventCount*LIST_SEP_Y + FONT_HEADING.height,
                    dateString,
                    &FONT_TEXT,
                    FLUSH_LEFT);

        DrawRectangle(BORDER,
                      LIST_START_Y + eventCount*LIST_SEP_Y + LIST_LINE_OFF,
                      LIST_LINE_W,
                      LIST_LINE_H);

        calendar = calendar->next;
        eventCount++;
    }
}

/**
 * @brief Displays the time and date the calender was synced in the bottom
 * right corner
 */
void ShowUpdateTime(void) {
    char time[20];
    tm_t currentTime = GetCurrentTime();
    strftime(time, 20, "%d.%m. %H:%M ", &currentTime);
    PrintString(RES_X - BORDER - ICON_UPDATE.width,
                RES_Y - BORDER - FONT_TEXT.height,
                time,
                &FONT_TEXT,
                FLUSH_RIGHT);

    PlaceIcon(RES_X - BORDER - ICON_UPDATE.width,
              RES_Y - BORDER - ICON_UPDATE.height,
              &ICON_UPDATE);
}

/**
 * @brief Marks an event on the calender
 * @param cellStart first cell the event covers (starts with 0)
 * @param cellEnd  last cell the event takes place
 */
static void MarkEvent(int32_t cellStart, int32_t cellEnd) {
    static uint32_t events[WEEKS * WEEK_DAYS];

    // event starts/ends outside calendar scope
    if ((cellStart < 0) && (cellEnd > 0)) {
        cellStart = 0;
        DrawRectangle(BORDER + LINE_W,
                      BORDER + CAL_SPACING + LINE_W,
                      CAL_SPACING,
                      EVENT_MARKER);
    }
    if (cellEnd > WEEK_DAYS*WEEKS - 1) {
        cellEnd = WEEK_DAYS*WEEKS - 1;
        if (cellStart <= WEEK_DAYS*WEEKS - 1) {
            DrawRectangle(BORDER + WEEK_DAYS * CAL_CELL_SIZE - CAL_SPACING,
                          BORDER + LINE_W + CAL_SPACING + CAL_CELL_SIZE,
                          CAL_SPACING,
                          EVENT_MARKER);
        }
    }

    if (cellStart != cellEnd) {
        // event longer than 1 day
        for (int i = cellStart; i <= cellEnd; ++i) {
            events[i] += 3;
        }

        // line break?
        if ((cellStart < WEEK_DAYS) && (cellEnd >= WEEK_DAYS)) {
            DrawRectangle(BORDER + CAL_SPACING + LINE_W +
                          cellStart % WEEK_DAYS * CAL_CELL_SIZE,
                          BORDER + CAL_SPACING + LINE_W +
                          cellStart / WEEK_DAYS * CAL_CELL_SIZE,
                          (WEEK_DAYS - cellStart) * CAL_CELL_SIZE - CAL_SPACING,
                          EVENT_MARKER);
            DrawRectangle(BORDER,
                          BORDER + CAL_SPACING + LINE_W + CAL_CELL_SIZE,
                          (cellEnd - WEEK_DAYS + 1) * CAL_CELL_SIZE
                          - CAL_SPACING,
                          EVENT_MARKER);
        } else {
            DrawRectangle(BORDER + CAL_SPACING + LINE_W +
                          cellStart % WEEK_DAYS * CAL_CELL_SIZE,
                          BORDER + CAL_SPACING + LINE_W +
                          cellStart / WEEK_DAYS * CAL_CELL_SIZE,
                          (cellEnd - cellStart + 1) * CAL_CELL_SIZE -
                          2 * CAL_SPACING - LINE_W,
                          EVENT_MARKER);
        }

    } else if (events[cellStart] < EVENTS_MAX) {
        events[cellStart]++;
        DrawRectangle(BORDER + CAL_SPACING + LINE_W +
                      (EVENT_MARKER + CAL_SPACING) *
                      ((events[cellStart] - 1) % 3) +
                      cellStart % WEEK_DAYS * CAL_CELL_SIZE,
                      BORDER + CAL_SPACING + LINE_W +
                      (EVENT_MARKER + CAL_SPACING) *
                      ((events[cellStart] - 1) / 3) +
                      cellStart / WEEK_DAYS * CAL_CELL_SIZE,
                      EVENT_MARKER, EVENT_MARKER);
    }
}

/**
 * @brief Allows to display one message on the screen bottom
 * @param time time of message
 * @param message message string
 */
void ShowMessage(tm_t *time, char *message) {
    char buffer[60];
    strftime(buffer, 60, "%H:%M ", time);
    strcat(buffer, message);

    PrintString(BORDER,
                BORDER + MSG_START_Y,
                "Telegram",
                &FONT_HEADING,
                FLUSH_LEFT);
    PrintString(BORDER,
                BORDER + MSG_START_Y + FONT_HEADING.height,
                buffer,
                &FONT_TEXT,
                FLUSH_LEFT);
}

/******************************************************************************/