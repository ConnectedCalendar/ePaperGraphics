/**
 *******************************************************************************
 * @file    Graphics.c
 * @brief   A monochrome extremly basic 2D drawing library
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include "Graphics.h"
#include "Font/Font.h"
#include "images/Icons.h"
#include <stdbool.h>
#include <string.h>

/* Private variables */
static uint8_t framebuffer[RES_CELLS];

/* Private functions */
static void PlaceBitmap(uint32_t x, uint32_t y, uint32_t height, uint32_t width,
                        const char *bitmap);

/******************************************************************************/
/**
 * @brief Checks whether coordinate is in frame
 * @param x x-coordinate
 * @param y y-coordinate
 * @return true if in frame
 */
uint8_t IsInFrame(uint32_t x, uint32_t y){
    if ((x < RES_X) && (y < RES_Y)) {
        return true;
    } else {
        return false;
    }
}

/**
 * @brief Draws a line (horizontal or vertical) with define line width
 * @param x top left x-coordinate
 * @param y top left y-coordinate
 * @param direction 0: horizontal, 1: vertical
 * @param length length of the line
 */
void DrawLine(uint32_t x, uint32_t y, uint8_t direction, uint32_t length) {
    if (direction == 0) {
        DrawRectangle(x, y, length, LINE_W);
    } else {
        DrawRectangle(x, y, LINE_W, length);
    }
}

/**
 * @brief Draws a filled rectangle
 * @param x top left x-coordinate
 * @param y top left y-coordinate
 * @param width width of the rectangle
 * @param height height of the rectangle
 */
void DrawRectangle(uint32_t x, uint32_t y, uint32_t width, uint32_t height) {
    uint8_t cell;
    uint8_t cellPos = 0;

    if (!IsInFrame(x + width-1, y + height-1)) {
        return;
    }

    for (uint32_t j = 0; j < width; ) {
        cell = 0;
        do {
            cell |= (1 << ((x + j) % 8));
            j++;
        } while ((j < width) && ((x + j) % 8 != 0));

        for (int i = 0; i < height; ++i) {
            framebuffer[x / 8 + cellPos + (y + i) * RES_CELLS_X] |= cell;
        }

        cellPos++;
    }
}

/**
 * @brief Inverts value within rectangular area
 * @param x top left x-coordinate
 * @param y top left y-coordinate
 * @param width width of the rectangle
 * @param height height of the rectangle
 */
void InvertArea(uint32_t x, uint32_t y, uint32_t width, uint32_t height) {
    uint8_t cell;
    uint8_t cellPos = 0;

    if (!IsInFrame(x + width-1, y + height-1)) {
        return;
    }

    for (uint32_t j = 0; j < width; ) {
        cell = 0;
        do {
            cell |= (1 << ((x + j) % 8));
            j++;
        } while ((j < width) && ((x + j) % 8 != 0));

        for (int i = 0; i < height; ++i) {
            framebuffer[x / 8 + cellPos + (y + i) * RES_CELLS_X] ^= cell;
        }

        cellPos++;
    }
}

/**
 * @brief Prints a string of a given font on the screen
 * @param x top left/right x-coordinate
 * @param y top left/right y-coordinate
 * @param str string to print
 * @param font pointer to font
 */
void PrintString(uint32_t x, uint32_t y, const char *str, const font_t *font,
                 flush_t alignment) {
    uint32_t cursor = 0;
    uint32_t len = strlen(str);

    if (alignment == FLUSH_LEFT) {
        for (int i = 0; i < len; ++i) {
            PlaceBitmap(x + cursor,
                        y,
                        font->height,
                        font->c[CharRemap(str[i])].width,
                        font->c[CharRemap(str[i])].bitmap);
            cursor += font->c[CharRemap(str[i])].width;
        }
    } else {
        for (int i = 0; i < len; ++i) {
            cursor += font->c[CharRemap(str[len - i - 1])].width;
            PlaceBitmap(x - cursor,
                        y,
                        font->height,
                        font->c[CharRemap(str[len - i - 1])].width,
                        font->c[CharRemap(str[len - i - 1])].bitmap);
        }
    }
}

/**
 * @brief Places icon on screen
 * @param x top left x-coordinate
 * @param y top left y-coordinate
 * @param icon pointer to icon structure
 */
void PlaceIcon(uint32_t x, uint32_t y, const icon_t *icon) {
    PlaceBitmap(x, y, icon->height, icon->width, icon->bitmap);
}

/**
 * @brief Get the address of the frame buffer
 * @return Pointer to the frame buffer
 */
uint8_t *GetFramebuffer(void) {
    return framebuffer;
}


/**
 * @brief Deletes all data in the framebuffer
 */
void ClearFramebuffer(void) {
    for (int i = 0; i < RES_CELLS; ++i) {
        framebuffer[i] = 0;
    }
}

/**
 * @brief Places bitmap on screen (used to print characters or placing images)
 * @param x top left x-coordinate
 * @param y top left y-coordinate
 * @param height image height in px
 * @param width image width in px
 * @param bitmap pointer to byte oriented monochrome bitmap
 */
static void PlaceBitmap(uint32_t x, uint32_t y, uint32_t height, uint32_t width,
                        const char *bitmap) {
    uint8_t cell;
    uint8_t cellPos;
    uint8_t bitPos = 0;
    uint32_t bytePos = 0;

    if (!IsInFrame(x + width-1, y + height-1)) {
        return;
    }

    for (int i = 0; i < height; ++i) {
        cellPos = 0;
        for (uint32_t j = 0; j < width;) {
            cell = 0;
            do {
                if (bitmap[bytePos] & (1 << bitPos % 8)) {
                    cell |= 1 << ((x + j) % 8);
                }
                j++;
                bitPos++;

                if (bitPos >= width) {
                    bitPos = 0;
                    bytePos++;
                } else if (bitPos % 8 == 0) {
                    bytePos++;
                }
            } while ((j < width) && ((x + j) % 8 != 0));

            framebuffer[x / 8 + cellPos + (y + i) * RES_CELLS_X] |= cell;

            cellPos++;
        }
    }
}

/******************************************************************************/