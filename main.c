/**
 *******************************************************************************
 * @file    main.c
 *******************************************************************************
 * Copyright 2018 Stefan Luethi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *******************************************************************************
 */

#include <stdio.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <time.h>
#include "Display.h"
#include "Graphics.h"
#include "UI.h"
#include "Calendar.h"
#include "Parser.h"

/* Private variables */
char image[RES_CELLS];

/* Private functions */

/******************************************************************************/

void init(void) {
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glClearColor(1.0, 1.0, 1.0, 0.0); // bg color
}

char ReverseBits(char byte) {
    return (char)(((byte & (1 << 0)) << 7) |
                  ((byte & (1 << 1)) << 5) |
                  ((byte & (1 << 2)) << 3) |
                  ((byte & (1 << 3)) << 1) |
                  ((byte & (1 << 4)) >> 1) |
                  ((byte & (1 << 5)) >> 3) |
                  ((byte & (1 << 6)) >> 5) |
                  ((byte & (1 << 7)) >> 7));
}

void ImageCopy(const char source[], char destination[]) {
    for (int i = 0; i < RES_Y; ++i) {
        for (int j = 0; j < RES_CELLS_X; ++j) {
            destination[j + RES_CELLS_X*i] =
                    ReverseBits(source[j + RES_CELLS_X*(RES_Y-1 - i)]);
        }
    }
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(1063, 1320);
    glutInitWindowPosition(3000, 50);
    glutCreateWindow(argv[0]);
    init();

    ClearFramebuffer();
    ClearCalendar();

    // stream reader
    FILE * file = fopen("../transmission.txt", "r");
    char line[350];

    while (fgets(line, sizeof(line), file)) {
        Parse(line);
    }

    fclose(file);

    DrawCalendar();
    ListEvents();
    ShowUpdateTime();
    //ShowMessage(&timeInfo, "The red brown fox jumps over the lazy, Stefan");

    ImageCopy(GetFramebuffer(), image);
    Display(image);
    return 0;
}

/******************************************************************************/