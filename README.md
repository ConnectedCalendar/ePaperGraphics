# ePaperGraphics
A simulator using OpenGL to develt a simple graphics library. The funtions allow
to parse a ASCII string (from the WiFi module containing the calendar data) and
display a graphical overview over the coming two weeks. The next few events are
also listed with time and date. The output is a monochromatic bitmap used for
the e-paper display.

## Prerequisites
The software was developed using JetBrains CLion as IDE on a linux system.
The following packages are required for OpenGL to work.
```
freeglut3
freeglut3-dev
mesa-utils
```

## Makefile adjustments
```
find_package (OpenGL REQUIRED)
find_package (GLUT REQUIRED)
include_directories(${OPENGL_INCLUDE_DIR}  ${GLUT_INCLUDE_DIRS})
target_link_libraries(ePaperDisplay ${OPENGL_LIBRARIES} ${GLUT_LIBRARY})
```
